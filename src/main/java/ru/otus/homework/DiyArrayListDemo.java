package ru.otus.homework;

import java.util.Collections;
import java.util.Comparator;

public class DiyArrayListDemo {
    public static void main(String[] args) {
        DiyArrayList<Integer> diyArrayList1 = new DiyArrayList<>();
        DiyArrayList<Integer> diyArrayList2 = new DiyArrayList<>();
        for (int i = 0; i < 20; i++) {
            diyArrayList1.add(i);
            diyArrayList2.add(30 - i);
        }
        Collections.addAll(diyArrayList1, 77, 78, -5);
        Collections.addAll(diyArrayList2, 0);
        Collections.copy(diyArrayList1, diyArrayList2);
        Collections.sort(diyArrayList1, Comparator.reverseOrder());
        for (Integer element : diyArrayList1) {
            System.out.print(element + " ");
        }
        System.out.println();
        for (Integer element : diyArrayList2) {
            System.out.print(element + " ");
        }
        System.out.println(diyArrayList1.contains(78));
        System.out.println(diyArrayList1.contains(745));
    }
}




