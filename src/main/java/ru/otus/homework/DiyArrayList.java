package ru.otus.homework;

import java.util.*;

public class DiyArrayList<T> implements List<T> {
    private static final int DEFAULT_CAPACITY = 10;
    private Object[] elementData;
    private int size;
    private final Object[] EMPTY_ARRAY = {};

    public DiyArrayList() {
        this.elementData = new Object[DEFAULT_CAPACITY];
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (Object element : elementData) {
            if (o.equals(element)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new DiyArrayListIterator();
    }

    @Override
    public Object[] toArray() {
        return Arrays.copyOf(elementData, size);
    }

    @Override
    public <E> E[] toArray(E[] a) {
        unsupported();
        return null;
    }

    @Override
    public boolean add(T t) {
        if (size == elementData.length) grow(size + 1);
        elementData[size] = t;
        size++;
        return true;
    }

    @Override
    public boolean remove(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(elementData[i])) {
                int newSize = size-1;
                System.arraycopy(elementData, i + 1, elementData, i, newSize - i);
                elementData[size = newSize] = null;
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        unsupported();
        return false;
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        unsupported();
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        unsupported();
        return false;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        unsupported();
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        unsupported();
        return false;
    }

    @Override
    public void clear() {
        unsupported();

    }

    @Override
    public T get(int index) {
        return (T) elementData[index];
    }

    @Override
    public T set(int index, T element) {
        Objects.checkIndex(index, size);
        T oldValue = (T) elementData[index];
        elementData[index] = element;
        return oldValue;
    }

    @Override
    public void add(int index, T element) {
        if (index > size || index < 0 ) {
            throw new IndexOutOfBoundsException(String.format("%d out of bound", index));
        }
        if (size == elementData.length) {
            grow(size + 1);
        }
        System.arraycopy(elementData, index, elementData, index + 1, size - index);
        elementData[index] = element;
        size++;
    }

    @Override
    public T remove(int index) {
        if (index > size || index < 0 ) {
            throw new IndexOutOfBoundsException(String.format("%d out of bound", index));
        }
        int newSize = size-1;
        T result = (T) elementData[index];
        System.arraycopy(elementData, index + 1, elementData, index, newSize - index);
        size = newSize;
        return result;
    }

    @Override
    public int indexOf(Object o) {
        for (int i = 0; i < size; i++) {
            if (o.equals(elementData[i])) {
                return i;
            }
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        unsupported();
        return 0;
    }

    @Override
    public ListIterator<T> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<T> listIterator(int index) {
        return new DiyArrayListListIterator();
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        unsupported();
        return null;
    }

    private void grow(int minCapacity) {
        int oldCapacity = elementData.length;
        if (oldCapacity > 0) {
            int newCapacity = oldCapacity * 2;
            elementData = Arrays.copyOf(elementData, newCapacity);
        } else {
            elementData = new Object[Math.max(DEFAULT_CAPACITY, minCapacity)];
        }
    }

    private void unsupported() {
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        String methodName = stackTrace[2].getMethodName();
        throw new UnsupportedOperationException(methodName + " is unsupported");
    }

    // iterator
    private class DiyArrayListIterator implements Iterator<T> {
        private int cursor;

        @Override
        public boolean hasNext() {
            return cursor < size;
        }

        @Override
        public T next() {
            return get(cursor++);
        }
    }

    private class DiyArrayListListIterator implements ListIterator<T> {
        private int cursor;
        private int lastRet = -1;

        @Override
        public boolean hasNext() {
            return cursor < size;
        }

        @Override
        public T next() {
            lastRet = cursor;
            T result = get(cursor);
            cursor++;
            return result;
        }

        @Override
        public boolean hasPrevious() {
            return cursor > 0;
        }

        @Override
        public T previous() {
            cursor--;
            lastRet = cursor;
            return get(cursor);
        }

        @Override
        public int nextIndex() {
            return cursor;
        }

        @Override
        public int previousIndex() {
            return cursor - 1;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

        @Override
        public void set(T t) {
            DiyArrayList.this.set(lastRet, t);
        }

        @Override
        public void add(T t) {
            throw new UnsupportedOperationException();
        }
    }

}
