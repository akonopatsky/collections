package ru.otus.homework;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import static org.junit.jupiter.api.Assertions.*;

class DiyArrayListTest {
    private DiyArrayList<Integer> diyArrayList1;
    private DiyArrayList<Integer> diyArrayList2;
    private Integer[] array1;
    private Integer[] array2;

    @BeforeEach
    void setUp() {
        diyArrayList1 = new DiyArrayList<>();
        diyArrayList2 = new DiyArrayList<>();
        array1 = new Integer[20];
        array2 = new Integer[20];
        for (int i = 0; i < 20; i++) {
            diyArrayList1.add(i);
            diyArrayList2.add(30 - i);
            array1[i] = i;
            array2[i] = 30 - i;
        }
    }

    @Test
    void size() {
        assertEquals(diyArrayList1.size(), 20);
    }

    @Test
    void isEmpty() {
        assertFalse(diyArrayList1.isEmpty());
        DiyArrayList<Integer> empty = new DiyArrayList<>();
        assertTrue(empty.isEmpty());
    }

    @Test
    void contains() {
        assertTrue(diyArrayList1.contains(5));
        assertFalse(diyArrayList1.contains(12344));
    }

    @Test
    void add() {
        diyArrayList1.add(555);
        assertTrue(diyArrayList1.contains(555));
    }

    @Test
    void addInPlace() {
        Assertions.assertThrows(IndexOutOfBoundsException.class, () -> diyArrayList1.add(25, 25));
        diyArrayList1.add(8, 88);
        assertEquals(21, diyArrayList1.size());
        assertEquals(88, diyArrayList1.get(8));
    }

    @Test
    void remove() {
        assertEquals(diyArrayList1.remove(10), 10);
        assertFalse(diyArrayList1.contains(10));
        assertEquals(19, diyArrayList1.size());
    }


    @Test
    void get() {
        assertEquals(12, diyArrayList1.get(12));
    }

    @Test
    void set() {
        diyArrayList1.set(7, 777);
        assertEquals(777, diyArrayList1.get(7));
        assertEquals(20, diyArrayList1.size());
    }

    @Test
    void indexOf() {
        assertEquals(14, diyArrayList1.indexOf(14));
    }

    @Test
    void toArray() {
        assertArrayEquals(array1, diyArrayList1.toArray());
    }

    @Test
    void collectionsOperation() {
        assertArrayEquals(array1, diyArrayList1.toArray());
        Collections.addAll(diyArrayList1, 20, 21, 22);
        assertEquals(23, diyArrayList1.size());
        Integer[] newArray1 = new Integer[23];
        System.arraycopy(array1, 0, newArray1, 0, 20);
        newArray1[20] = 20;
        newArray1[21] = 21;
        newArray1[22] = 22;
        assertArrayEquals(newArray1, diyArrayList1.toArray());
        Collections.copy(diyArrayList1, diyArrayList2);
        Collections.sort(diyArrayList1, Comparator.reverseOrder());
        System.arraycopy(array2, 0, newArray1, 0, 20);
        Arrays.sort(newArray1, Comparator.reverseOrder());
        assertArrayEquals(newArray1, diyArrayList1.toArray());
    }
}